CApp.controller('IndexController', function($scope,$http,$location) {
        $scope.selectEvent = function(id){
            $location.path( "/event/" + id );
        }

        var url = '/a/user/events';
        $http({method: 'GET', url: url}).
            success(function(data, status, headers, config) {
                $http({method: 'GET', url: '/a/user'}).
                    success(function(me, status, headers, config) {
                        data.events.forEach(function(entry){
                            if( entry.creator_id == me._id ){
                                entry.action = 'glyphicon glyphicon-play';
                                entry.color = 'panel panel-success';
                            }else{
                                entry.action = 'glyphicon glyphicon-minus-sign';
                                entry.color = 'panel panel-default';
                            }
                            entry.attendeesCount = entry.attendees.length;
                        });
                    });
                $scope.events = data.events;

            });
});

CApp.controller('AllEventController', function($scope,$http,$location) {
    $scope.search = function(query){
        //should get all data
        $scope.events=[];
    };

    $scope.searchName = 'Search';
    $scope.searchBy = function(By){
        $scope.searchName = By;
    };

    var url = '/a/events';
    $http({method: 'GET', url: url}).
        success(function(data, status, headers, config) {
            $http({method: 'GET', url: '/a/user'}).
                success(function(me, status, headers, config) {
                    data.forEach(function(entry){
                        entry.color = 'panel panel-default';
                        if( entry.creator_id == me._id ){
                            entry.action = 'glyphicon glyphicon-pencil';
                            entry.color = 'panel panel-success';
                            entry.target = 'me';

                        }
                        entry.attendeesCount = entry.attendees.length;
                    });
                });
            $scope.events = data;
        });

    $scope.selectEvent = function(id,target){
        if(target=='me'){
            $location.path( "/event/" + id );
        }else{
        //    $scope.eventId = {eventId:id};
        //    $http.post('/a/event/join', JSON.stringify($scope.eventId)).success(function(){
                $location.path( "/event/" + id );
        //    });
        }
    }
});


CApp.controller('EventController', function ($scope, $http, $routeParams, $templateCache, $location, localStorageService, socket, $rootScope) {

    $scope.actual_question = {text:'Waiting for the event to be live ...'};

    //Socket IO incoming requests
    socket.on('on:user:join', function(data){
        console.log(data);
    });
    socket.on('on:user:leave', function(data){
        console.log(data);
    });
    socket.on('on:show:question', function(data){
        $scope.actual_question = data;
        //console.log($scope.actual_question);
    });

	var URL = '/a/event/' + $routeParams.id;

    $scope.goStatisticsQuestion = function(qlistId,questionId){
        var url = '/a/graph?eventId='+$scope.event._id+'&qlistId='+qlistId+'&questionId='+questionId;
        $http({method:'GET', url:url})
            .success(function(data,status){
                //console.log(data);
                $rootScope.qGraphData = data;
                $location.path( "/statistics/question/"+questionId);
            })
            .error(function(data,status){

            });
    }
    $scope.goStatistics = function(id){
        var url = '/a/qgraph?eventId='+$scope.event._id+'&qlistId='+id;
        $http({method:'GET', url:url})
            .success(function(data,status){
                $rootScope.qlistGraphData = data;
                $location.path( "/statistics/questionnaire/"+id);
            })
            .error(function(data,status){

            });
    }

    //EventController Initialization
	$scope.init = function () {
		$http({method: 'GET', url: URL})
			.success(function (data, status) {
				$scope.event = data;

				$http({method: 'GET', url: '/a/user', cache: $templateCache})
					.success(function (data, status) {
						$scope.user = data;

                        //Join only if user is an attendee
                        if($scope.event.creator_id != $scope.user._id)
                            socket.emit('user:join',{userId:$scope.user._id,eventId:$scope.event._id});

						if ($scope.event.creator_id == $scope.user._id) {
							$scope.is_owner = true;
						}
						else {
							$scope.is_owner = false;

							//Check if I'm on the attendees list
							if ($scope.event.attendees.indexOf($scope.user._id) == -1) {
								$scope.joined = false;
							}
							else {
								$scope.joined = true;
							}
						}
					})
					.error(function (data, status) {
						$scope.user = {
							name: 'ERROR!',
							error: status
						}
					});
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR!',
					error: status
				}
			});
	};

    //Check when the user leaves the page
    $scope.$on('$locationChangeStart',function(event, next, current){
        //Leave only if user is an attendee
        if($scope.event.creator_id != $scope.user._id)
            socket.emit('user:leave',{userId:$scope.user._id,eventId:$scope.event._id});
    });

    //Send question to the attendees
    $scope.sendQuestionLive = function(){
       socket.emit('show:question',{userId:$scope.user._id,eventId:$scope.event._id});
    };
    $scope.sendPreviousQuestionLive = function(){
        socket.emit('show:previous:question',{userId:$scope.user._id,eventId:$scope.event._id});
    };
    $scope.startQList = function(qlistId){
        $scope.isLive = true;
        if($scope.event.creator_id == $scope.user._id){
            socket.emit('user:join',{userId:$scope.user._id,eventId:$scope.event._id});
            socket.emit('set:qlist',{userId:$scope.user._id,eventId:$scope.event._id,qlistId:qlistId});
        }
    };
    $scope.stopQList = function(qlistId){
        this.isLive = false;
        if($scope.event.creator_id == $scope.user._id){
            socket.emit('user:leave',{userId:$scope.user._id,eventId:$scope.event._id});
            socket.emit('stop:qlist',{userId:$scope.user._id,eventId:$scope.event._id,qlistId:qlistId});
        }
    };

	$scope.saveQList = function () {
		var postURL = '/a/event/qlist/';
		var questionnaire = {eventId: $scope.event._id, name: $scope.qlistName, questions: [], deleted: false};
		$http({method: 'POST', url: postURL, data: questionnaire})
			.success(function (data, status) {
				if (data.error != null)
					alert(data.error);
				else {
					$scope.event.qlists.push(questionnaire);
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR!',
					error: status
				}
			});
	};

	$scope.saveQuestion = function (qText, qlistId) {
		var random = Math.random().toString(36).substring(7);
		localStorageService.add(qlistId + '.' + random, qText);

		var postURL = '/a/event/qlist/question';
		var question = { eventId: $scope.event._id, qlistId: qlistId, text: qText, variants: [], answer: 1, deleted: false};
		$http({method: 'POST', url: postURL, data: question})
			.success(function (data, status) {
				if (data.error != null) {
					alert(data.error);
				}
				else {
					for (var i = 0; i < $scope.event.qlists.length; i++) {
						if ($scope.event.qlists[i]._id == qlistId) {
							$scope.event.qlists[i].questions.push({text: qText, variants: [], answer: 1, deleted: false})
							break;
						}
					}
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR!',
					error: status
				}
			});

	};

	$scope.saveVariant = function (variantText, questionId, qlistId) {
		var postURL = '/a/event/qlist/question/variant';
		var variant = {eventId: $scope.event._id, qlistId: qlistId, questionId: questionId, text: variantText};
		$http({method: 'POST', url: postURL, data: variant})
			.success(function (data, status) {
				if (data.error != null) {
					alert(data.error);
				}
				else {
					for (var i = 0; i < $scope.event.qlists.length; i++) {
						if ($scope.event.qlists[i]._id == qlistId) {
							for (var j = 0; j < $scope.event.qlists[i].questions.length; j++) {
								if ($scope.event.qlists[i].questions[j]._id == questionId) {
									$scope.event.qlists[i].questions[j].variants.push(variantText);
                                    console.log(data);
									break;
								}
							}
						}

					}
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR',
					error: status
				}
			});
	};

	$scope.deleteQList = function (qlistId) {
		var url = '/a/event/qlist/delete/' + $scope.event._id;
		var req = {qlistId: qlistId};
		$http({method: 'POST', url: url, data: req})
			.success(function (data, status) {
				if (data.error != null) {
					alert(data.error);
				}
				else {
					$scope.event = data;
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR',
					error: status
				}
			});
	};

	$scope.deleteQuestion = function (questionId, qlistId) {
		var url = '/a/event/qlist/question/delete/' + $scope.event._id;
		var req = {qlistId: qlistId, questionId: questionId};
		$http({method: 'POST', url: url, data: req})
			.success(function (data, status) {
				if (data.error != null) {
					alert(data.error);
				}
				else {
					for (var i = 0; i < $scope.event.qlists.length; i++) {
						if ($scope.event.qlists[i]._id == qlistId) {
							for (var j = 0; j < $scope.event.qlists[i].questions.length; j++) {
								if ($scope.event.qlists[i].questions[j]._id == questionId) {
									$scope.event.qlists[i].questions.splice(j, 1);
									break;
								}
							}
						}
					}
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR',
					error: status
				}
			});
	};

	$scope.deleteVariant = function (text, questionId, qlistId) {
		var url = '/a/event/qlist/question/variant/delete/' + $scope.event._id;
		var req = {qlistId: qlistId, questionId: questionId, text: text};
		$http({method: 'POST', url: url, data: req})
			.success(function (data, status) {
				if (data.error != null) {
					alert(data.error);
				}
				else {
					for (var i = 0; i < $scope.event.qlists.length; i++) {
						if ($scope.event.qlists[i]._id == qlistId) {
							for (var j = 0; j < $scope.event.qlists[i].questions.length; j++) {
								if ($scope.event.qlists[i].questions[j]._id == questionId) {
									for (var k = 0; k < $scope.event.qlists[i].questions[j].variants.length; k++) {
										if ($scope.event.qlists[i].questions[j].variants[k] == text) {
											$scope.event.qlists[i].questions[j].variants.splice(k, 1);
											break;
										}
									}
								}
							}
						}
					}
				}
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR',
					error: status
				}
			});
	};

    $scope.saveCorrectAnswer = function(questionid,qlistid,index){
        var postURL = '/a/event/qlist/question/variantanswer/';
        var answer = {eventId: $scope.event._id, qlistId: qlistid, questionId: questionid, correctAnswer: index};
        $http({method: 'POST', url: postURL, data: answer})
            .success(function (data, status) {
                if (data.error != null) {
                    alert(data.error);
                }
                else {
                    console.log("correct save");
                }
            })
            .error(function (data, status) {
                $scope.event = {
                    name: 'ERROR',
                    error: status
                }
            });
        $scope.correctIndex = -1;
    };

    $scope.saveAnswer = function(questionId,index){
            var data = {index:index, userId:$scope.user._id,eventId:$scope.event._id,questionId:questionId};
            console.log(data);
            socket.emit('save:answer',data);
    }

    $scope.deleteCorrectAnswer = function(){
        console.log("Deleting correct answer ... ");
    };

	$scope.join = function (eventId, userId) {
		var postURL = '/a/event/join/';
		var info = {eventId: eventId};
		$http({method: 'POST', url: postURL, data: info})
			.success(function (data, status) {
				if (data.error != null)
					alert(data.error);
				$location.path("/");
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR!',
					error: status
				};
				$location.path("/");
			});
	};

	$scope.leave = function (eventId, userId) {
		var postURL = '/a/event/leave/';
		var info = {eventId: eventId};
		$http({method: 'POST', url: postURL, data: info})
			.success(function (data, status) {
				if (data.error != null)
					alert(data.error);
				$location.path("/");
			})
			.error(function (data, status) {
				$scope.event = {
					name: 'ERROR!',
					error: status
				};
				$location.path("/");
			});
	}
});

CApp.controller('facebookUserController', function ($scope, $http, $location, $templateCache) {
	$scope.user = null;

	$http({method: 'GET', url: '/a/user', cache: $templateCache})
		.success(function (data, status) {
			$scope.user = data;
		})
		.error(function (data, status) {
			$scope.user = {
				name: 'ERROR!',
				error: status
			}
		});
});

CApp.controller('menuController', function ($scope, $location) {
	$scope.selectMenu = function (menu) {
		if (menu == 'myevent') {
			$location.path("/");
		} else if (menu == 'allevent') {
			$location.path("/allevents");
		} else if (menu == 'admin') {
			$location.path("/admin");
		} else if (menu == 'createEvent') {
			$location.path("/createEvent");
		}
	};
	$scope.isActive = function (viewLocation) {
		return viewLocation === $location.path();
	};
});

CApp.controller('adminController', function ($scope, $http, $location) {

	var url = '/a/users';
	$http({method: 'GET', url: url}).
		success(function (data, status, headers, config) {
			$scope.users = data;

		});


    $scope.selectUser = function (id) {
        target = 'edituser';
        $location.path("/" + target + "/" + id);
    }
    $scope.moveForAdd = function () {
        target = 'adduser';
        $location.path("/" + target);
    }
    $scope.removeUser = function (id) {

        $scope.user={_id:''};

        $scope.user._id = id;

        $http.post('/a/userRemove', JSON.stringify($scope.user)).success(function(){
        });

    }
});


CApp.controller('edituserController', function ($scope, $http, $location) {

	var locationUrl = window.location.href;

	var StrBack = "";
	if (locationUrl != '') {
		gg = locationUrl.split("/");
		var MaxI = gg.length;
		StrBack = gg[MaxI - 1];
	}


	var url = '/a/user/get/' + StrBack;

	$http({method: 'GET', url: url}).
		success(function (data, status, headers, config) {
			$scope.user = data;
		});

	$scope.return = function () {
		$location.path("/admin");
	}

    $scope.updateUser = function(){


        $scope.newUser={_id:'',name:'',facebookId:''};

        $scope.newUser._id = $scope.user._id;
        $scope.newUser.name = $scope.name;
        $scope.newUser.facebookId = $scope.facebookId;


        $http.post('/a/userUpdate', JSON.stringify($scope.newUser)).success(function(){
            //$location.path( "/");
        });

    }


});

CApp.controller('adduserController', function ($scope, $http, $location) {

    $scope.addUser = function(){


        $scope.user={name:'',facebookId:''};

        $scope.user.name = $scope.name;
        $scope.user.facebookId = $scope.facebookId;

        $http.post('/a/userAdd', JSON.stringify($scope.user)).success(function(){
        });

        $location.path("/admin");

    }
    $scope.return = function () {

        $location.path("/admin");
    }

});

CApp.controller('createEventController',function($scope,$http,$location){
    $scope.tags = [{t:''},{t:''},{t:''}];
    $scope.addTag = function(){
        $scope.tags.push({t:''});
    };

    $scope.event={name:'',description:'',date:'',time:'',tags:[]};
    $scope.createEvent = function() {

        $scope.event.name = $scope.name;
        $scope.event.description = $scope.description;
        $scope.event.date = $scope.date;
        $scope.event.time = $scope.time;

        $scope.tags.forEach(function(entry){
            if(entry.t!=''){
                $scope.event.tags.push(entry.t);
            }
        });

        $http.post('/a/event', JSON.stringify($scope.event)).success(function(){
            $location.path( "/");
        });
    };
});

CApp.controller('statisticsQuestionnaireController',function($scope,$routeParams,$rootScope){

    var data = [];
    for (var i = 0; i < $rootScope.qlistGraphData.length; i++) {
        data.push({
            c: [
                {v: i},
                {v: $rootScope.qlistGraphData[i].correct},
                {v: $rootScope.qlistGraphData[i].correct.toString()},
                {v: $rootScope.qlistGraphData[i].incorrect},
                {v:$rootScope.qlistGraphData[i].incorrect.toString()}
            ]
        });
    }

    var chart1 = {};
    chart1.type = "AreaChart";
    chart1.displayed = true;
    chart1.cssStyle = "height:450px; width:100%;";
    chart1.data = {"cols": [
        {id: "score", label: "Score", type: "string"},
        {id: "userscount", label: "Count of User", type: "number"}
    ], "rows": [
        {c: [
            {v: "0"},
            {v: 1, f: "42 items"}
        ]},
        {c: [
            {v: "1"},
            {v: 3}
        ]},
        {c: [
            {v: "2"},
            {v: 5}
        ]},
        {c: [
            {v: "3"},
            {v: 10}
        ]},
        {c: [
            {v: "4"},
            {v: 13}
        ]},
        {c: [
            {v: "5"},
            {v: 25}
        ]},
        {c: [
            {v: "6"},
            {v: 36}
        ]},
        {c: [
            {v: "7"},
            {v: 45}
        ]},
        {c: [
            {v: "8"},
            {v: 23}
        ]},
        {c: [
            {v: "9"},
            {v: 5}
        ]},
        {c: [
            {v: "10"},
            {v: 4}

        ]}
    ]};

    chart1.options = {
        "title": "Score Distribution",
        "color":"['#004411']",
        "isStacked": "false",
        "fill": 30,
        "displayExactValues": true,
        "vAxis": {
            "title": "Number of users", "gridlines": {"count": 5}
        },
        "hAxis": {
            "title": "Score"
        }
    };


    var formatCollection = []

    chart1.formatters = {};

    var chart2 = {};
    chart2.type = "AreaChart";
    chart2.displayed = true;
    chart2.cssStyle = "height:450px; width:100%;";
    chart2.data = {"cols": [
        {id: "question", label: "Question", type: "string"},
        {id: "Cuser", label: "Correct User", type: "number"},
        {id: "tag1", label: "tag1", type: "string",role: "annotation"},
        {id: "Wuser", label: "Wrong User", type: "number"},
        {id: "tag2", label: "tag2", type: "string",role: "annotation"}
    ], "rows": data};


    chart2.options = {
        "title": "Correct rate by Question",
        "isStacked": "false",
        "fill": 30,
        "displayExactValues": true,
        "vAxis": {
            "title": "Correct rate", "gridlines": {"count": 5}
        },
        "hAxis": {
            "title": "Question"
        }
    };


    var formatCollection = []

    chart2.formatters = {};

    $scope.chartlist = [];
    $scope.chartlist.push(chart1);
    $scope.chartlist.push(chart2);

    $scope.Title = $routeParams.id ;
    $scope.formatCollection = formatCollection;


    $scope.chartReady = function () {
        fixGoogleChartsBarsBootstrap();
    }

    function fixGoogleChartsBarsBootstrap() {
        // Google charts uses <img height="12px">, which is incompatible with Twitter
        // * bootstrap in responsive mode, which inserts a css rule for: img { height: auto; }.
        // *
        // * The fix is to use inline style width attributes, ie <img style="height: 12px;">.
        // * BUT we can't change the way Google Charts renders its bars. Nor can we change
        // * the Twitter bootstrap CSS and remain future proof.
        // *
        // * Instead, this function can be called after a Google charts render to "fix" the
        // * issue by setting the style attributes dynamically.

        $(".google-visualization-table-table img[width]").each(function (index, img) {
            $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
    };

});


CApp.controller('statisticsQuestionController',function($scope,$routeParams,$rootScope){

    var data = [];
    for (var i = 0; i < $rootScope.qGraphData.length; i++) {
        data.push({
           c: [
               {v: i},
               {v: $rootScope.qGraphData[i]}
           ]
        });
    }

    var chart1 = {};
    chart1.type = "BarChart";
    chart1.displayed = true;
    chart1.cssStyle = "height:300px; width:100%;";
    chart1.data = {"cols": [
        {id: "score", label: "Score", type: "string"},
        {id: "userscount", label: "Count of Users", type: "number"}
    ], "rows": data};

    chart1.options = {
        "title": "Answer",
        "color":"['#004411']",
        "isStacked": "false",
        "fill": 30,
        "pieSliceText":"label",
        "displayExactValues": true,
        "vAxis": {
            "title": "Answer"
        },
        "hAxis": {
            "title": "Number of User"
        }
    };


    var formatCollection = []

    chart1.formatters = {};


    $scope.chartlist = [];
    $scope.chartlist.push(chart1);

    $scope.formatCollection = formatCollection;

    //$scope.Title = $routeParams.id ;

    $scope.chartReady = function () {
        fixGoogleChartsBarsBootstrap();
    }

    function fixGoogleChartsBarsBootstrap() {
        // Google charts uses <img height="12px">, which is incompatible with Twitter
        // * bootstrap in responsive mode, which inserts a css rule for: img { height: auto; }.
        // *
        // * The fix is to use inline style width attributes, ie <img style="height: 12px;">.
        // * BUT we can't change the way Google Charts renders its bars. Nor can we change
        // * the Twitter bootstrap CSS and remain future proof.
        // *
        // * Instead, this function can be called after a Google charts render to "fix" the
        // * issue by setting the style attributes dynamically.

        $(".google-visualization-table-table img[width]").each(function (index, img) {
            $(img).css("width", $(img).attr("width")).css("height", $(img).attr("height"));
        });
    };

});
