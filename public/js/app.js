angular.module('LocalStorageModule').value('prefix', 'CApp');

var CApp = angular.module('CApp',['ngRoute', 'LocalStorageModule','googlechart']);

//Route Provider definition
CApp.config(function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl:'views/index.html',
            controller:'IndexController'
        })
        .when('/allevents',{
            templateUrl:'views/allevents.html',
            controller:'AllEventController'
        })
        .when('/event/:id',{
            templateUrl:'views/event.html',
            controller:'EventController'
        })
        .when('/admin',{
            templateUrl:'views/admin.html',
            controller:'adminController'
        })
        .when('/edituser/:id',{
            templateUrl:'views/edituser.html',
            controller:'edituserController'
        })
        .when('/adduser',{
            templateUrl:'views/adduser.html',
            controller:'adduserController'
        })
        .when('/createEvent',{
            templateUrl:'views/createEvent.html',
            controller:'createEventController'
        })
        .when('/statistics/question/:id',{
            templateUrl:'views/statistics.html',
            controller:'statisticsQuestionController'
        })
        .when('/statistics/questionnaire/:id',{
            templateUrl:'views/statistics.html',
            controller:'statisticsQuestionnaireController'
        })
        .otherwise({redirectTo:'/'})
	});


//Socket definition
CApp.factory('socket', function($rootScope) {
    var socket = io.connect();
    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if(callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
});
