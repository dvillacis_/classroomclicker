/**
 * Created by Вова on 11/15/13.
 */
CApp
    .directive('event', function() {
        return {
            restrict:'E',
            templateUrl: 'templates/event.html'
        }})
    .directive('searchbar', function() {
        return {
            restrict:'E',
            templateUrl: 'templates/searchbar.html'
        }})

    .directive('user', function() {
        return {
            restrict:'E',
            templateUrl: 'templates/user.html'
        }})
    .directive('questionnaire', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/questionnaire.html'
        }})
    .directive('question', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/question.html'
        }})
    .directive('livevariant', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/live_variant.html'
        }})
    .directive('variant', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/variant.html'
        }})
    .directive('eventdet', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/eventDetailed.html'
        }

});