var express = require('express')
	, http = require('http')
	, path = require('path')
	, mongoose = require('mongoose')
	, schemas = require('./schemas')
	, passport = require('passport')
	, facebook = require('./facebook')

/* ROUTES */
	, index = require('./routes/index')
	, user = require('./routes/user')
	, event = require('./routes/event')
	, qlist = require('./routes/qlist')
	, question = require('./routes/question')
    , variant = require('./routes/variant')
    , answer = require('./routes/answer')
	, io = require('socket.io')

//	, secret = 'clickclick'
//	, app = express()
//	, server = http.createServer(app)
//	, io = io.listen(server)

	, connect = require('express/node_modules/connect')

//	, cookieParser = express.cookieParser(secret)
//	, sessionStore = new connect.middleware.session.MemoryStore()
    , socketHelper = require('./routes/socketHelper');

mongoose.connect('mongodb://root:root@ds063297.mongolab.com:63297/clicker');

var ClickerApp = function(){
    var self = this;

    /*  ================================================================  */
    /*  Helper functions.                                                 */
    /*  ================================================================  */

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || 3000;

        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };

    /**
     *  Retrieve entry (content) from cache.
     *  @param {string} key  Key identifying content to retrieve from cache.
     */
    self.cache_get = function(key) { return self.zcache[key]; };


    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
    self.terminator = function(sig){
        if (typeof sig === "string") {
            console.log('%s: Received %s - terminating sample app ...',
                Date(Date.now()), sig);
            process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()) );
    };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
    self.setupTerminationHandlers = function(){
        //  Process on exit and signals.
        process.on('exit', function() { self.terminator(); });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
            'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
                process.on(element, function() { self.terminator(element); });
            });
    };

    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function(){
        self.secret = 'clickclick';
        self.app = express();
        self.server = http.createServer(self.app);
        self.io = io.listen(self.server);
        self.cookieParser = express.cookieParser(self.secret);
        self.sessionStore = new connect.middleware.session.MemoryStore();
        
        self.app.set('views', __dirname + '/views');
        self.app.engine('html', require('ejs').renderFile);
        self.app.use(express.json());
        self.app.use(express.urlencoded());
        self.app.use(express.logger('tiny'));
        self.app.use(self.cookieParser);
        self.app.use(express.bodyParser());
        self.app.use(express.methodOverride());
        self.app.use(express.session({store: self.sessionStore, key: self.secret}));
        self.app.use(passport.initialize());
        self.app.use(passport.session());
        self.app.use(self.app.router);
        self.app.use(express.static(path.join(__dirname, 'public')));

        self.app.get('/', index.index);

        self.app.get('/login', function (req, res) {
            //currently useless
            res.redirect('/')
        });

        self.app.get('/auth/facebook', passport.authenticate('facebook'));
        self.app.get('/auth/facebook/callback',
            passport.authenticate('facebook', { failureRedirect: '/login' }),
            function (req, res) {
                res.redirect('/');
            });
        self.app.get('/auth/out', facebook.logout);

        //User Management
        self.app.get('/a/user', facebook.ensureAuthenticated, user.me);
        self.app.get('/a/users', user.getUsers);
        self.app.get('/a/user/get/:id', facebook.ensureAuthenticated, user.get);
        self.app.get('/a/user/events', facebook.ensureAuthenticated, user.getEvents);
        self.app.delete('/a/user/events', facebook.ensureAuthenticated, user.deleteEvent);
        self.app.post('/a/userAdd', facebook.ensureAuthenticated, user.post);
        self.app.post('/a/userRemove', facebook.ensureAuthenticated, user.remove);
        self.app.post('/a/userUpdate', facebook.ensureAuthenticated, user.update);

        //Event Management
        self.app.post('/a/event', facebook.ensureAuthenticated, event.post);
        self.app.post('/a/event/join', facebook.ensureAuthenticated, event.join);
        self.app.post('/a/event/leave', facebook.ensureAuthenticated, event.leave);
        self.app.get('/a/events', facebook.ensureAuthenticated, event.list);
        self.app.get('/a/event/:id', facebook.ensureAuthenticated, event.get);

        //Questionnaires Management
        self.app.post('/a/event/qlist/', facebook.ensureAuthenticated, qlist.post);
        self.app.get('/a/event/qlist/:id', facebook.ensureAuthenticated, qlist.get);
        self.app.post('/a/event/qlist/delete/:id', facebook.ensureAuthenticated, qlist.remove);
        self.app.get('/a/qgraph', facebook.ensureAuthenticated, qlist.graph);

        //Questions Management
        self.app.post('/a/event/qlist/question', facebook.ensureAuthenticated, question.post);
        self.app.get('/a/event/qlist/question/:id', facebook.ensureAuthenticated, question.get);
        self.app.post('/a/event/qlist/question/delete/:id', facebook.ensureAuthenticated, question.remove);
        self.app.post('/a/event/qlist/question/variantanswer', facebook.ensureAuthenticated, question.postanswer);
        self.app.get('/a/graph', facebook.ensureAuthenticated, question.graph);

        //Variant Management
        self.app.post('/a/event/qlist/question/variant', facebook.ensureAuthenticated, variant.post);
        self.app.post('/a/event/qlist/question/variant/delete/:id', facebook.ensureAuthenticated, variant.remove);

        //Socket IO event listeners
        self.io.set('log level',1);
        self.io.sockets.on('connection',function(socket){
            socket.on('user:join',function(data){
                socketHelper.joinEvent(socket,data);
            });
            socket.on('user:leave',function(data){
                socketHelper.leaveEvent(socket,data);
            });
            socket.on('show:question',function(data){
                socketHelper.showNextQuestion(socket,data);
            });
            socket.on('show:previous:question',function(data){
                socketHelper.showPreviousQuestion(socket,data);
            });
            socket.on('set:qlist', function(data){
                socketHelper.selectQlist(socket,data);
            });
            socket.on('save:answer',function(data){
                socketHelper.saveAnswer(socket,data);
            });
        });
    }

    /**
     *  Initializes the clicker application.
     */
    self.initialize = function() {
        self.setupVariables();
        self.setupTerminationHandlers();

        // Create the express server and routes.
        self.initializeServer();
    };


    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        //  Start the app on the specific interface (and port).
        self.server.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...',
                Date(Date.now() ), self.ipaddress, self.port);
        });
    };
}



var capp = new ClickerApp();
capp.initialize();
capp.start();

