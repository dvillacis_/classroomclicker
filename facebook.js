var schemas = require('./schemas')
	, p = require('passport')
	, FacebookStrategy = require('passport-facebook').Strategy;

p.serializeUser(function(user, done) {
	done(null, user);
});

p.deserializeUser(function(obj, done) {
	done(null, obj);
});

p.use(new FacebookStrategy({
        clientID: '607118266015547',
        clientSecret: '2fc05679c2b47ab614832afd08aa0590',
        callbackURL: "http://localhost:3000/auth/facebook/callback"
	},

	function(accessToken, refreshToken, profile, done) {
		process.nextTick(function () {
			schemas.User.findOrCreate({ facebookId : profile.id }, {name: profile.displayName}, function(err, user) {
				user.authDate = Date.now();
				user.save( function(e) {
					return done( null, user );
				});
			})
		});
	}
));


exports.passport = p;
exports.ensureAuthenticated = function(req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	res.redirect('/login')
}

exports.logout = function(req, res) {
	req.logout();
	res.redirect('/');
}