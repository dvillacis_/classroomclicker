var schemas = require('../schemas');

exports.me = function(req, res){
	schemas.User.findOne({ facebookId: req.user.facebookId }, function(e, u) {
		res.send(u);
	});
};

exports.get = function(req, res) {
	schemas.User.findOne({ facebookId: req.params.id }, function(e, u){
		res.send(u);
	});
};

exports.getUsers = function(req,res){
    schemas.User.find({},function(e,u){
        res.send(u);
    })
}

exports.getEvents = function(req, res) {
	schemas.User.findOne({ facebookId: req.user.facebookId }, function(e, u) {
		schemas.Event.find({ creator_id: u._id, deleted : false}, function(er, events) {
            schemas.Event.find({attendees: u._id},function(error,newevents){
                events = events.concat(newevents);
                res.send({
                    count: events.length,
                    events: events,
                    error: er
                });
            });
		});
    });
}

exports.deleteEvent = function(req, res) {
	schemas.User.findOne({ facebookId: req.user.facebookId }, function(err, u) {
		schemas.Event.findById(req.body._id, function(err,ev){
			ev.deleted = true;
			ev.save(function(e){
				console.log( e ? e : 'event deleted');
			});
		});
		res.send('');
	});
}

exports.post = function(req,res){
    var user = new schemas.User({
        name				: req.body.name
        , facebookId		: req.body.facebookId

});
user.save(function (err) {
    if (!err) console.log('Success!');
});

}

exports.remove = function(req,res){
    var user = new schemas.User({
        _id : req.body._id
    });
    user.remove(function (err) {
        if (!err) console.log('Success delete!');
    });
}


exports.update = function(req,res){

    var user = new schemas.User({
        _id : req.body._id
        , name : req.body.name
        , facebookId : req.body.facebookId
    });

    var conditions = { _id: req.body._id }
        , update = { $set: { name: req.body.name }}
        , options = {};

    user.update(conditions, update, options, function (err) {

        if (!err) console.log('Success update!');

    });

}


