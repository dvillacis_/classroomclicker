/**
 * Created by david on 30/11/13.
 */
var schemas = require('../schemas');
var users = [];

exports.joinEvent = function(socket, data){
    users.push(data.userId);
    schemas.Event.findById(data.eventId,function(error,event){
        if(event.creator_id == data.userId){
            socket.counter = 0;
            //console.log("Initializing counter for socket ...");
        }

    });
    socket.users = users;
    socket.room = data.eventId;
    socket.join(data.eventId);
    socket.emit('on:user:join','>>SERVER:'+data.userId+ ' you are connected to event '+data.eventId);
    console.log(">> User joined live event ");
};

exports.leaveEvent = function(socket,data){
    users.splice(users.indexOf(data.userId),1);
    socket.leave(data.eventId);
    socket.emit('on:user:leave','>>SERVER:'+data.userId+ ' you are disconnected to event '+data.eventId);
    console.log(">> User left live event "+ data.eventId);
};

var sendQuestionLive = function(socket,data){
    //console.log(socket.users);
    schemas.Event.findById(data.eventId,function(error,event){
        socket.broadcast.to(socket.room).emit('on:show:question',event.qlists[socket.qlistIndex].questions[socket.counter]);
        socket.counter = socket.counter+1;
        //console.log(socket.counter);
    });
}
exports.showPreviousQuestion = function(socket,data){
    //console.log(socket.users);
    schemas.Event.findById(data.eventId,function(error,event){
        if(socket.counter != 0)
            socket.counter = socket.counter-1;
        socket.broadcast.to(socket.room).emit('on:show:question',event.qlists[socket.qlistIndex].questions[socket.counter]);
        //console.log(socket.counter);
    });
};

exports.selectQlist = function(socket,data){
    schemas.Event.findById(data.eventId,function(error,event){
        if(event.creator_id == data.userId){
            socket.counter = 0;
            socket.qlistIndex = 0;
            console.log("Initializing counter for socket ...");
        }
        for(var i = 0; i < event.qlists.length; i++){
            if(event.qlists[i]._id == data.qlistId){
                socket.qlistIndex = i;
                sendQuestionLive(socket,data);
                break;
            }
        }
    });
};

exports.showNextQuestion = function(socket,data){
    sendQuestionLive(socket,data);
}

exports.saveAnswer = function(socket, data){
    schemas.Event.findById(data.eventId,function(error, event){
        if(event.attendees.indexOf(data.userId) != -1){
            for(var i = 0; i<event.qlists.length; i++){
                for(var j = 0; j < event.qlists[i].questions.length;j++){
                    if(event.qlists[i].questions[j]._id == data.questionId){
                        var contains = false;
                        for(var k = 0; k<event.qlists[i].questions[j].answers.length; k++){
                            if(event.qlists[i].questions[j].answers[k].userId == data.userId){
                                event.qlists[i].questions[j].answers.splice(k,1);
                            }
                        }
                        var answer = new schemas.Answer({
                            choice:data.index,
                            userId:data.userId,
                            isCorrect:(event.qlists[i].questions[j].correct_answer == data.index)
                        })
                        event.qlists[i].questions[j].answers.push(answer);
                        break;
                    }
                }
            }
            console.log(event.qlists[0].questions);
            event.save(function(e){
                console.log( e ? e : 'answer added');
            });
        }
    });
};