exports.index = function(req, res){
	if (req.isAuthenticated()) {
		res.render('index.ejs');
	} else {
		res.render('login.ejs');
	}
};