/**
 * Created by david on 11/11/13.
 */

var schemas = require('../schemas');

exports.post = function(req,res){
    var question = new schemas.Question({
        text:req.body.text,
        variants:req.body.variants,
        answer:req.body.answer,
        deleted:false
    });
    schemas.Event.findById(req.body.eventId,function(err,ev){
        for(var i=0; i < ev.qlists.length; i++){
            if(ev.qlists[i]._id == req.body.qlistId){
                ev.qlists[i].questions.push(question);
                ev.save(function(error){
                    console.log(error ? error : 'question added');
                });
                break;
            }
        }
        res.send(ev);
    });
};

exports.get = function(req,res){
    //TODO: Make sure the attendee does not receive the answer for the question
};

exports.remove = function(req,res){
    schemas.Event.findById(req.params.id,function(err,ev){
        if ( ev.creator_id == req.user._id ) {
            for ( var i = 0; i < ev.qlists.length; i++){
                if ( ev.qlists[i]._id == req.body.qlistId){
                    for(var j=0; j < ev.qlists[i].questions.length; j++){
                        if(ev.qlists[i].questions[j]._id == req.body.questionId){
                            ev.qlists[i].questions.splice(j,1);
                            ev.save(function(e){
                                console.log( e ? e : 'question deleted');
                            });
                            break;
                        }
                    }
                }
            }
        }
        //console.log(ev.qlists);
        res.send(ev);
    });
};

exports.graph = function(req,res){
    console.log("Getting statistics!!!");
    var eventId = req.query.eventId;
    var qlistId = req.query.qlistId;
    var questionId = req.query.questionId;

    schemas.Event.findById(eventId,function(error, event){

        for(var i = 0; i<event.qlists.length; i++){
            if(event.qlists[i]._id == qlistId){
                for(var j = 0; j<event.qlists[i].questions.length; j++){
                    if(event.qlists[i].questions[j]._id == questionId){
                        var results = [];
                        for (var o = 0; o<event.qlists[i].questions[j].variants.length;o++){
                            results[o] = 0;
                        }
                        console.log(results);
                        for(var k = 0; k<event.qlists[i].questions[j].answers.length;k++){
                            var x = event.qlists[i].questions[j].answers[k];
                            results[x.choice]++;
                        }
                        console.log(results);
                        res.send(results);
                    }
                }
            }
        }

    });
};

exports.postanswer = function(req,res){
    schemas.Event.findById(req.body.eventId,function(err,ev){
        for(var i=0; i < ev.qlists.length; i++){
            if(ev.qlists[i]._id == req.body.qlistId){
                for(var j=0; j<ev.qlists[i].questions.length;j++){
                    if(ev.qlists[i].questions[j]._id == req.body.questionId){

                        ev.qlists[i].questions[j].correct_answer = req.body.correctAnswer;
                        ev.save(function(error){
                            console.log(error ? error : 'correctAnswer added');
                        });
                        break;


                    }
                }
            }
        }
        res.send(ev);
    });
};