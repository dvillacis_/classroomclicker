var schemas = require('../schemas');


exports.list = function(req, res){
	schemas.Event.find({}, function(e, event) {
		res.send(event);
	});
};

exports.get = function(req, res) {
	schemas.Event.findById(req.params.id, function(e, event){
		res.send(event);
	});

};

exports.join = function(req,res){
    schemas.Event.findById(req.body.eventId,function(err,ev){
        if ( ev.owner != req.user._id ) {
            ev.attendees.push(req.user._id);
            ev.save(function(e){
                console.log(e ? e : 'user joined event');
            });
            res.send(ev);
        } else {
            res.send({error: "You are the owner of the event"});
        }
    });
};

exports.leave = function(req,res){
    schemas.Event.findById(req.body.eventId, function(err,ev){
        if(ev.owner != req.user._id){
            var index = ev.attendees.indexOf(req.user._id);
            ev.attendees.splice(index,1);
            ev.save(function(e){
               console.log(e ? e : 'user left event');
            });
            res.send(ev);
        }  else {
            res.send({error: "You are the owner of the event"});
        }
    });
};

exports.post = function(req,res){
	var date = new Date(req.body.date + ' ' + req.body.time);
	var event = new schemas.Event({
		creator_id: req.user._id
		, creator_name: req.user.name
		, name: req.body.name
		, description: req.body.description
		, date : date
		, creationDate: Date.now()
        , tags: req.body.tags
        , isOpen: false
		, deleted : false
	});

	event.save(function() {
		schemas.User.findOne({facebookId: req.user.facebookId},function(err,user){
			user.events.push(event._id);
			user.save( function(e){
				console.log( e ? e : 'saved new event');
			});
		});
	});

	res.redirect('/');
};