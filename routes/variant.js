/**
 * Created by david on 24/11/13.
 */
var schemas = require('../schemas');

exports.post = function(req,res){
    schemas.Event.findById(req.body.eventId,function(err,ev){
        for(var i=0; i < ev.qlists.length; i++){
            if(ev.qlists[i]._id == req.body.qlistId){
                for(var j=0; j<ev.qlists[i].questions.length;j++){
                    if(ev.qlists[i].questions[j]._id == req.body.questionId){
                        ev.qlists[i].questions[j].variants.push(req.body.text);
                        ev.save(function(error){
                            console.log(error ? error : 'variant added');
                        });
                        break;
                    }
                }
            }
        }
        res.send(ev);
    });
};

exports.remove = function(req,res){
    schemas.Event.findById(req.params.id,function(err,ev){
        if ( ev.creator_id == req.user._id ) {
            for ( var i = 0; i < ev.qlists.length; i++){
                if ( ev.qlists[i]._id == req.body.qlistId){
                    for(var j=0; j < ev.qlists[i].questions.length; j++){
                        if(ev.qlists[i].questions[j]._id == req.body.questionId){
                            for(var k = 0; k < ev.qlists[i].questions[j].variants.length; k++){
                                if(ev.qlists[i].questions[j].variants[k] == req.body.text){
                                    ev.qlists[i].questions[j].variants.splice(k,1);
                                    ev.save(function(e){
                                        console.log( e ? e : 'variant deleted');
                                    });
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        //console.log(ev.qlists);
        res.send(ev);
    });
};