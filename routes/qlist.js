/**
 * Created by david on 11/11/13.
 */

var schemas = require('../schemas');

exports.post = function(req,res){
    var qlist = new schemas.QList({
        name:req.body.name,
        questions:req.body.questions,
        deleted:false
    });
    schemas.Event.findById(req.body.eventId,function(err,ev){
        if ( ev.creator_id == req.user._id ) {
			ev.qlists.push(qlist);
			ev.save(function(e){
				console.log(e ? e : 'qlist added');
			});

			res.send(ev);
		} else {
			res.send({error: "You are not an owner of the event"});
		}
    });
};

exports.get = function(req,res){
    schemas.Event.findById(req.params.id,function(err,ev){
		if ( ev.owner != req.user._id ) {
			for ( var i = 0; i < ev.qlists.length; i++ ) {
				for ( var j = 0; j < ev.qlists[i].length; j++ ) {
					delete ev.qlists[i][j].answer;
				}
			}
		}
        res.send(ev.qlists);
    });
};

exports.remove = function(req,res){
    console.log(req.body);
    schemas.Event.findById(req.params.id,function(err,ev){
        if ( ev.creator_id == req.user._id ) {
            for ( var i = 0; i < ev.qlists.length; i++){
                if ( ev.qlists[i]._id == req.body.qlistId){
                    //ev.qlists[i].deleted = true;
                    ev.qlists.splice(i,1);
                    ev.save(function(e){
                        console.log( e ? e : 'qlist deleted');
                    });
                    break;
                }
            }
        }
        res.send(ev);
    });
};

exports.graph = function(req,res){
    var eventId = req.query.eventId;
    var qlistId = req.query.qlistId;

    schemas.Event.findById(eventId,function(error, event){

        for(var i = 0; i<event.qlists.length; i++){
            if(event.qlists[i]._id == qlistId){
                var results = [];
                for (var o = 0; o < event.qlists[i].questions.length; o++){
                    results[o] = {
                        correct: 0,
                        incorrect: 0
                    };
                    for(var j = 0; j < event.qlists[i].questions[o].answers.length; j++){
                        var x = event.qlists[i].questions[o].answers[j];
                        if ( x.isCorrect == true) {
                            results[o].correct++;
                        } else {
                            results[o].incorrect++;
                        }

                    }
                }
                console.log(results);
                res.send(results);
            }
        }

    });
};