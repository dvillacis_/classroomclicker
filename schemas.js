var mongoose = require('mongoose')
	, Schema = mongoose.Schema
	, ObjectId = Schema.ObjectId
	, findOrCreate = require('mongoose-findorcreate');

var QuestionSchema = new Schema({
		text				: String
		, variants			: [String]
		, correct_answer	: Number
        , deleted           : Boolean
        , answers           : [AnswerSchema]
	})
    , AnswerSchema = new Schema({
        userId              : ObjectId
        , isCorrect         : Boolean
        , choice            : Number
    })
	, QListSchema = new Schema({
		name				: String,
		questions			: [QuestionSchema],
        deleted             : Boolean
	})

	, UserSchema = new Schema({
		name				: String
		, facebookId		: { type: String,  required: true }
		, registrationDate	: { type: Date, default: Date.now }
		, authDate			: { type: Date, default: Date.now }
		, events			: [ObjectId]
		, groups 			: [ObjectId]
	})

	, GroupSchema = new Schema({
		name 				: String
		, count				: Number
	})

	, EventSchema = new Schema({
		creator_name		: String
		, creator_id		: ObjectId
		, name				: { type: String, required: true }
		, description		: { type: String }
		, date				: { type: Date, required: true}
		, creationDate		: { type: Date, default: Date.now, required:true }
		, attendees			: [ObjectId]
		, qlists			: [QListSchema]
        , tags              : [String]
        , isOpen            : Boolean
		, deleted 			: Boolean
	});

UserSchema.plugin(findOrCreate);

exports.User = mongoose.model( 'User',	UserSchema);
exports.Event = mongoose.model( 'Event', EventSchema);
exports.QList = mongoose.model( 'QList', QListSchema);
exports.Question = mongoose.model( 'Question', QuestionSchema);
exports.Answer = mongoose.model( 'Answer', AnswerSchema);
